import Home from "./Home"
import ContactUs from "./ContactUs";
import Footer from "./Footer"
import ContactButton from "./ContactButton"
import Navbar from "./Navbar"
import Services from "./Services"
import "../node_modules/bootstrap/dist/css/bootstrap.min.css";
import "bootstrap/dist/js/bootstrap.bundle"

function App() {
  return (
    <>
    <Navbar></Navbar>
    <Home></Home>
    {/* <ContactButton></ContactButton> */}
    <Services></Services>
    <ContactUs></ContactUs>
    <Footer></Footer>
    </>
  );
}

export default App;
