import React from "react"

const Navbar = () => {
    return (
        <>
            <div className="Navbar">
                <input id="page-nav-toggle" class="main-navigation-toggle" type="checkbox" />
                    <label for="page-nav-toggle">
                    <svg class="icon--menu-toggle" viewBox="0 0 60 30">
                        <g class="icon-group">
                        <g class="icon--menu">
                            <path d="M 6 0  60 0" />
                            <path d="M 6 15 L 60 15" />
                            <path d="M 6 30 L 60 30" />
                        </g>
                        <g class="icon--close">
                            <path d="M 20 0 L 60 30" />
                            <path d="M 20 30 L 60 0" />
                        </g>
                        </g>
                    </svg>
                    </label>

                    <nav class="main-navigation">
                    <ul>
                        <li><a href="#0">Home</a></li>
                        <li><a href="#0">Services</a></li>
                        <li><a href="#0">Our clients</a></li>
                        <li><a href="#0">About us</a></li>
                        <li><a href="#0">Contact us</a></li>
                        <li><a href="#0">Follow us</a></li>
                    </ul>
                    </nav>
                </div> 

        </>
    )
}

export default Navbar;