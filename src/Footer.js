import React from "react";
import FBlogo from "./SVG/FBlogo"
import Instalogo from "./SVG/Instalogo"
import Whatsapplogo from "./SVG/Whatsapplogo"

const Footer = () => {
    return (
        <>
<footer class="nb-footer footer">
<div class="container">
<div class="row">
<div class="col-sm-12">
<div class="about">
     
        
  <div class="social-btns">
      
      <a class="btn facebook" href="#">
        <FBlogo></FBlogo>
      </a>
      <a class="btn instagram" href="#">
          <Instalogo></Instalogo>
      </a>

      <a class="btn whatsapp" href="#">                
        <Whatsapplogo></Whatsapplogo>
      </a>

  </div>

</div>
</div>
</div>
</div>

    <section class="copyright">
        <div class="container  text-center">
        <div class="row">
            <div class="row-sm-6">
                <p>Bhubaneswar, Odisha, 751024</p>
            </div>
        <div class="row-sm-6">
            <p>Copyright © 2021. www.byteapp.com</p>
        </div>
        </div>
        </div>
    </section>
</footer>
        </>
    );
}

export default Footer;