import React from "react"
import Heroimg from "./SVG/Heroimg"
import HeroName from "./SVG/HeroName"

const Home = () => {
    return (
        <>
        <section id="hero" class="hero d-flex align-items-center">

            <div class="container">
            <div class="row">
                <div class="col-lg-6 d-flex flex-column justify-content-center">
                    <HeroName></HeroName>
                <h2 className="d-flex justify-content-start">Empowering thoughts with Code</h2>

                    <div class="text-center btn-get">
                    <a href="#about" class="btn-get-started d-flex justify-content-start text-decoration-none">
                        <span>Get Started</span>
                    </a>
                    </div>
                </div>
                <div class="col-lg-6 hero-img">
                    <Heroimg class="img-fluid"></Heroimg>
                </div>
            </div>
            </div>
        </section>
        </>
    )
}

export default Home;