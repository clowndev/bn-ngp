import React from "react";
import "./index.css"

const ContactUs = () => {
return (
  <>
    <section id="contact" class="contact">

    <div class="container" data-aos="fade-up">

      <header class="section-header">
        <h1 className="text-center">Contact Us</h1>
      </header>

  <div class="row gy-4 ">

    <div class="col-lg-6">
      <div class="col gy-4">
                <div class="row-md-6 text-center">
                  <div class="info-box">
                  <svg xmlns="http://www.w3.org/2000/svg" width="40" height="40" fill="#dc4a38" class="bi bi-geo-alt" viewBox="0 0 16 16">
                    <path d="M12.166 8.94c-.524 1.062-1.234 2.12-1.96 3.07A31.493 31.493 0 0 1 8 14.58a31.481 31.481 0 0 1-2.206-2.57c-.726-.95-1.436-2.008-1.96-3.07C3.304 7.867 3 6.862 3 6a5 5 0 0 1 10 0c0 .862-.305 1.867-.834 2.94zM8 16s6-5.686 6-10A6 6 0 0 0 2 6c0 4.314 6 10 6 10z"/>
                    <path d="M8 8a2 2 0 1 1 0-4 2 2 0 0 1 0 4zm0 1a3 3 0 1 0 0-6 3 3 0 0 0 0 6z"/>
                  </svg>
                    <h3>Address</h3>
                    <p>Bhubaneswar<br/>Odisha<br/>India, 751024</p>
                  </div>
                </div>

                          <br/>

                <div class="row-md-6 text-center">
                  <div class="info-box">
                  <svg xmlns="http://www.w3.org/2000/svg" width="40" height="40" fill="#dc4a38" class="bi bi-chat-quote" viewBox="0 0 16 16">
                  <path d="M2.678 11.894a1 1 0 0 1 .287.801 10.97 10.97 0 0 1-.398 2c1.395-.323 2.247-.697 2.634-.893a1 1 0 0 1 .71-.074A8.06 8.06 0 0 0 8 14c3.996 0 7-2.807 7-6 0-3.192-3.004-6-7-6S1 4.808 1 8c0 1.468.617 2.83 1.678 3.894zm-.493 3.905a21.682 21.682 0 0 1-.713.129c-.2.032-.352-.176-.273-.362a9.68 9.68 0 0 0 .244-.637l.003-.01c.248-.72.45-1.548.524-2.319C.743 11.37 0 9.76 0 8c0-3.866 3.582-7 8-7s8 3.134 8 7-3.582 7-8 7a9.06 9.06 0 0 1-2.347-.306c-.52.263-1.639.742-3.468 1.105z"/>
                  <path d="M7.066 6.76A1.665 1.665 0 0 0 4 7.668a1.667 1.667 0 0 0 2.561 1.406c-.131.389-.375.804-.777 1.22a.417.417 0 0 0 .6.58c1.486-1.54 1.293-3.214.682-4.112zm4 0A1.665 1.665 0 0 0 8 7.668a1.667 1.667 0 0 0 2.561 1.406c-.131.389-.375.804-.777 1.22a.417.417 0 0 0 .6.58c1.486-1.54 1.293-3.214.682-4.112z"/>
                  </svg>
                    <h3>Our Info</h3>
                    <p>++91 9938006060<br/>Information@byteapp.com</p>
                </div>
                </div>
      </div>
    </div>

    <div class="col-lg-6">
      <form action="forms/contact.php" method="post" class="php-email-form">
        <div class="row gy-4">

          <div class="col-md-12">
            <input type="text" class="form-control" name="Name" placeholder="Your Name" required/>
          </div>

          <div class="col-md-6">
            <input type="text" name="name" class="form-control" placeholder="name@email.com" required/>
          </div>

          <div class="col-md-6 ">
            <input type="email" class="form-control" name="email" placeholder="Phone No." required/>
          </div>

          <div class="col-md-12">
            <input type="text" class="form-control" name="subject" placeholder="Subject" required/>
          </div>

          <div class="col-md-12">
            <textarea class="form-control" name="message" rows="2" placeholder="Message" required></textarea>
          </div>

          <div class="col-md-12 text-center">
            <div class="loading">Loading</div>
            <div class="error-message"></div>
            <div class="sent-message">Your message has been sent. Thank you!</div>

            <button type="submit">Send Message</button>
          </div>

        </div>
      </form>

    </div>

  </div>

</div>

</section>
  </>
)
}
export default ContactUs;